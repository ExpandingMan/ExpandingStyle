# ExpandingStyle

[![docs](https://img.shields.io/badge/🗐-website-blue?style=for-the-badge)](https://ExpandingMan.gitlab.io/ExpandingStyle/)

A code style for the Julia language.  See link above.
